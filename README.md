# WSO2 APIM implementation



## Database
Using adminer, import the sql script from dbscripts. This will create the database, users etc required for APIM.

## Deploy APIM
Strat the docker container to work with GKE and k8s.

```
docker-compose run gsdk-wso2 /bin/bash 
```

```bash
cd /home


# Create config maps
# Analytics
kubectl create configmap apim-analytics-app-config --from-file=configmaps/analytics/deployment.yaml -n wso2
kubectl describe configmap apim-analytics-app-config
kubectl delete configmap apim-analytics-app-config



# Deploy WSO2 API Manager Analytics...'
kubectl apply -f apim-analytics-deployment.yaml -n wso2
kubectl delete -f apim-analytics-deployment.yaml -n wso2

kubectl apply -f apim-analytics-service.yaml -n wso2
kubectl delete -f apim-analytics-service.yaml -n wso2

# Deploy WSO2 API Manager

## Config maps

### default namespace
-- create

kubectl create configmap apim-app-config-wso2server --from-file=configmaps/apim/bin/wso2server.sh
kubectl create configmap apim-app-config-api-manager --from-file=configmaps/apim/repository/conf/api-manager.xml
kubectl create configmap apim-app-config-carbon --from-file=configmaps/apim/repository/conf/carbon.xml
kubectl create configmap apim-app-config-master-datasources --from-file=configmaps/apim/repository/conf/datasources/master-datasources.xml
kubectl create configmap apim-app-config-catalina-server --from-file=configmaps/apim/catalina-server.xml

-- delete

kubectl delete configmap apim-app-config-wso2server
kubectl delete configmap apim-app-config-api-manager
kubectl delete configmap apim-app-config-carbon
kubectl delete configmap apim-app-config-master-datasources
kubectl delete configmap apim-app-config-catalina-server


### wso2 namespace
-- create

kubectl create configmap apim-app-config-wso2server --from-file=configmaps/apim/bin/wso2server.sh -n wso2
kubectl create configmap apim-app-config-api-manager --from-file=configmaps/apim/repository/conf/api-manager.xml -n wso2
kubectl create configmap apim-app-config-carbon --from-file=configmaps/apim/repository/conf/carbon.xml -n wso2
kubectl create configmap apim-app-config-master-datasources --from-file=configmaps/apim/repository/conf/datasources/master-datasources.xml -n wso2
kubectl create configmap apim-app-config-catalina-server --from-file=configmaps/apim/catalina-server.xml -n wso2

-- delete

kubectl delete configmap apim-app-config-wso2server -n wso2
kubectl delete configmap apim-app-config-api-manager -n wso2
kubectl delete configmap apim-app-config-carbon -n wso2
kubectl delete configmap apim-app-config-master-datasources -n wso2
kubectl delete configmap apim-app-config-catalina-server -n wso2



# Deploy the pod and service
kubectl apply -f apim-deployment.yaml -n wso2
kubectl delete -f apim-deployment.yaml -n wso2

kubectl apply -f apim-service.yaml -n wso2
kubectl delete -f apim-service.yaml -n wso2


```

## Links after deployed

https://apigw.kumoanalytics.com:9443
https://apim.kumoanalytics.com:9443/services/TenantMgtAdminService?wsdl
https://apim.kumoanalytics.com:9443/services/RemoteUserStoreManagerService?wsdl


## Admin services
https://docs.wso2.com/display/Carbon420/Calling+Admin+Services+from+Apps


## Changing the hostname URL
https://docs.wso2.com/display/AM260/Changing+the+Hostname#example-apiURL

## Customising the API Store theme
https://docs.wso2.com/display/AM260/Adding+a+New+API+Store+Theme

## Saving costs
https://medium.com/google-cloud/cutting-costs-on-kubernetes-cluster-gke-concepts-applied-cc340eba0bec

## Disabling TLS
Shit do not work. To undo the change, either remove the vol map or copy the catalena file from the configmaps-backup

https://docs.wso2.com/display/ADMIN44x/Configuring+Transport+Level+Security#ConfiguringTransportLevelSecurity-EnablingTLSanddisablingSSLsupport

## Default ports

### Identity Server

9443 - HTTPS servlet transport (the default URL of the management console is https://localhost:9443/carbon)
9763 - HTTP servlet transport

### API Manager

7712 - Thrift SSL port for secure transport, where the client is authenticated to use WSO2 API-M Analytics.

7612 - Thrift TCP port where WSO2 API-M Analytics receives events from clients.
7444 - The default port for the Stream Processor Store API.

9444 - MSF4J HTTPS Port in the Stream Procesor. This is used by the Microgateway.

## WSO2 enginner to help
- disable tls for store, publisher, carbon and gateway
- what features I will not get when I use key manager and not IS
- If I start without IS for the MVP, how challanging it is to migrate to IS?
- How to change the default admin password at the time of deployment
- 

